<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = Category::get();

        return response()->json([
            'message' => 'Categories Retrieved Successfully.',
            'data' => $categories
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'enable' => 'required',
        ]);

        $category = new Category($request->all());

        if ($category->save()) {
            return response()->json([
                'message' => 'Category Stored Successfully.',
                'data' => $category
            ], 201);
        }

        return response()->json([
            'message' => 'Store Failed.'
        ], 400);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $category = Category::find($id);

        if (!$category) {
            return response()->json([
                'message' => 'Category Not Found.',
            ], 404);
        }

        return response()->json([
            'message' => 'Category Retrieved Successfully.',
            'data' => $category
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'enable' => 'required',
        ]);

        $category = Category::find($id);

        if (!$category) {
            return response()->json([
                'message' => 'Category Not Found.',
            ], 404);
        }

        $category->update($request->all());

        return response()->json([
            'message' => 'Category Updated Successfully.',
            'data' => $category
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if (!$category) {
            return response()->json([
                'message' => 'Category Not Found.',
            ], 404);
        }

        if ($category->delete()) {
            return response()->json([
                'message' => 'Category Deleted Successfully.',
                'data' => $category
            ], 200);
        }

        return response()->json([
            'message' => 'Delete Failed.'
        ], 400);
    }
}
