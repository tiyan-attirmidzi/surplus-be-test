<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Category;
use App\Models\Image;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::get();

        return response()->json([
            'message' => 'Products Retrieved Successfully.',
            'data' => $products
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'enable' => 'required',
            'categories' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $product = new Product($request->except('categories', 'images'));

        if ($product->save()) {
            if($request->has('categories')) {
                foreach ($request->categories as $category) {
                    $product->productCategories()->attach($category);
                }
            }

            if ($request->hasFile('images')) {
                foreach ($request->file('images') as $image) {
                    $image->storeAs('public/products', $image->hashName());

                    $productImage = Image::create([
                        'name' => "Image Product " . $product->name,
                        'file' => $image->hashName(),
                        'enable' => 1,
                    ]);

                    $product->productImages()->attach($productImage->id);
                }
            }

            return response()->json([
                'message' => 'Product Stored Successfully.',
                'data' => $product
            ], 201);
        }

        return response()->json([
            'message' => 'Store Failed.'
        ], 400);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $product = Product::with(['productCategories', 'productImages'])->find($id);

        if (!$product) {
            return response()->json([
                'message' => 'Product Not Found.',
            ], 404);
        }

        return response()->json([
            'message' => 'Product Retrieved Successfully.',
            'data' => $product
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'enable' => 'required',
            'categories' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $product = Product::with(['productCategories', 'productImages'])->find($id);

        if (!$product) {
            return response()->json([
                'message' => 'Product Not Found.',
            ], 404);
        }

        if ($product->update($request->except('categories', 'images'))) {
            if ($request->has('categories')) {
                $product->productCategories()->detach();
                foreach ($request->categories as $category) {
                    $product->productCategories()->attach($category);
                }
            }

            if ($request->hasFile('images')) {
                foreach ($product->productImages as $image) {
                    Storage::delete('public/products/'.$image->file);
                    Image::find($image->id)->delete();
                }

                $product->productImages()->detach();

                foreach ($request->file('images') as $image) {
                    $image->storeAs('public/products', $image->hashName());

                    $productImage = Image::create([
                        'name' => "Image Product " . $product->name,
                        'file' => $image->hashName(),
                        'enable' => 1,
                    ]);

                    $product->productImages()->attach($productImage->id);
                }
            }

            return response()->json([
                'message' => 'Product Updated Successfully.',
                'data' => $product
            ], 200);
        }

        return response()->json([
            'message' => 'Updated Failed.'
        ], 400);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $product = Product::with(['productCategories', 'productImages'])->find($id);

        if (!$product) {
            return response()->json([
                'message' => 'Product Not Found.',
            ], 404);
        }

        foreach ($product->productImages as $image) {
            Storage::delete('public/products/'.$image->file);
            Image::find($image->id)->delete();
        }

        $product->productCategories()->detach();
        $product->productImages()->detach();

        $product->delete();

        return response()->json([
            'message' => 'Product Deleted Successfully.',
            'data' => $product
        ], 200);
    }
}
