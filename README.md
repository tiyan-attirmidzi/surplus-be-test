# SURPLUS BE TEST

This is a Simple Rest API with [PHP](https://www.php.net/) using [Laravel](https://laravel.com/) framework and database using `MySQL`.

## Preparation

Install all the `Dependency` present in the project.
```bash
composer install
```

Duplicate the `.env.example` file.
```
cp .env.example .env
```

In the `.env` file, define the cridential database with your configuration.
```
DB_DATABASE=<YOUR_DATABASE_NAME>
DB_USERNAME=<YOUR_DATABASE_USERNAME>
DB_PASSWORD=<YOUR_DATABASE_PASSWORD>
```

Run Migration.
```
php artisan migrate
```

Run Seeder.
```
php artisan db:seed
```

Run the development server.
```
php artisan serve
```

## API Documentation

Open this *[link](https://elements.getpostman.com/redirect?entityId=7067675-536de1d6-401e-441e-878f-3ab874bbaa6d&entityType=collection)*, to view the API Documentation.

